from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Friend
from .views import index, friend_list, get_friend_list, add_friend, delete_friend, validate_npm
from .api_csui_helper.csui_helper import CSUIhelper
import json

class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_get_friend_list(self):
        Friend.objects.create(friend_name="name", npm="12332113232")
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_lab_7_with_invalid_page(self):
        response = Client().get('/lab-7/?page=100')
        self.assertEqual(response.status_code, 302)

    def test_lab7_can_create_new_friend(self):
    	Friend.objects.create(friend_name="name", npm="123321123321")

    	self.assertEqual(Friend.objects.all().count(), 1)

    def test_lab7_post_success(self):
        response_post = Client().post('/lab-7/add-friend/', {'name': 'name', 'npm': '1606234566'})
        self.assertEqual(response_post.status_code, 200)
        self.assertEqual(Friend.objects.all().count(), 1)

    def test_lab7_post_duplicate_npm(self):
        Friend.objects.create(friend_name="name", npm="123321123321")
        response_post = Client().post('/lab-7/add-friend/', {'name': 'name', 'npm': '123321123321'})
        self.assertEqual(response_post.status_code, 400)
        self.assertEqual(Friend.objects.all().count(), 1)

    def test_lab7_delete_success(self):
        friend = Friend(friend_name="name", npm="1606234566")
        friend.save()

        response = Client().post('/lab-7/delete-friend/' + str(friend.id) + '/')
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Friend.objects.all().count(), 0)

    def test_csui_helper(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

    def test_csui_helper_wrong_password(self):
        with self.assertRaises(Exception):
            csui_helper2 = CSUIhelper(username="heho", password="hohoe")

    def test_validate_npm_not_exist(self):
        response_post = Client().post('/lab-7/validate-npm/', {'npm': '1606234566'})
        self.assertEqual(response_post.status_code, 200)
