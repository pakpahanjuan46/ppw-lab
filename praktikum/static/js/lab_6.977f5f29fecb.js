// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
      print.value = "";
      erase = false;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'sin') {
      print.value = Math.round(Math.sin(evil(print.value) * Math.PI / 180) * 10000) / 10000;
      erase = true;
  } else if (x === 'log') {
      print.value = Math.round(Math.log10(evil(print.value)) * 10000) / 10000;
      erase = true;
  } else if (x === 'tan') {
      print.value = Math.round(Math.tan(evil(print.value) * Math.PI / 180) * 10000) / 10000;
      erase = true;
  } else {
      if (erase) {
        print.value = "";
        erase = false;
      }
      print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

//SELECT THEME
localStorage.setItem("themes", JSON.stringify([
    {"id": 0, "text": "Red", "bcgColor": "#F44336", "fontColor": "#FAFAFA"},
    {"id": 1, "text": "Pink", "bcgColor": "#E91E63", "fontColor": "#FAFAFA"},
    {"id": 2, "text": "Purple", "bcgColor": "#9C27B0", "fontColor": "#FAFAFA"},
    {"id": 3, "text": "Indigo", "bcgColor": "#3F51B5", "fontColor": "#FAFAFA"},
    {"id": 4, "text": "Blue", "bcgColor": "#2196F3", "fontColor": "#212121"},
    {"id": 5, "text": "Teal", "bcgColor": "#009688", "fontColor": "#212121"},
    {"id": 6, "text": "Lime", "bcgColor": "#CDDC39", "fontColor": "#212121"},
    {"id": 7, "text": "Yellow", "bcgColor": "#FFEB3B", "fontColor": "#212121"},
    {"id": 8, "text": "Amber", "bcgColor": "#FFC107", "fontColor": "#212121"},
    {"id": 9, "text": "Orange", "bcgColor": "#FF5722", "fontColor": "#212121"},
    {"id": 10, "text": "Brown", "bcgColor": "#795548", "fontColor": "#FAFAFA"}
]));



$(document).ready(function() {
  $('.my-select').select2({
    'data': JSON.parse(localStorage.getItem('themes'))
  })

  if(localStorage.selectedTheme === null){
    localStorage.setItem("selectedTheme", JSON.stringify({"Indigo": {"bcgColor": "#3F51B5", "fontColor": "#FAFAFA"}}));
    var selectedTheme = JSON.parse(localStorage.selectedTheme);
    $('body').css({"backgroundColor": selectedTheme['bcgColor']});
    $('.text-center').css({"color": selectedTheme['fontColor']});
  }
  else{
    var selectedTheme = JSON.parse(localStorage.selectedTheme);
    $('body').css({"backgroundColor": selectedTheme['bcgColor']});
    $('.text-center').css({"color": selectedTheme['fontColor']});
  }
  
  $('.apply-button').on('click', function(){  // sesuaikan class button
    var theme = JSON.parse(localStorage.getItem('themes'))[$('.my-select').val()];//karena id dari 0, maka id sesuai index
    $('body').css({"backgroundColor": theme['bcgColor']});
    $('.text-center').css({"color": theme['fontColor']});
    localStorage.setItem('selectedTheme', JSON.stringify(theme));
  })

});
//END
