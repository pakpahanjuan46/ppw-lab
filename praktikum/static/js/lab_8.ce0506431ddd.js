// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '549544325383614',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });


    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login

      FB.getLoginStatus(response => {
        render(response.status === 'connected');
      })
    };

    // Call init facebook. default dari facebook
    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));

    const render = loginFlag => {
      if (loginFlag) {
        getUserData(user => {
          console.log(user);
          $('#lab8').html(
            '<div class="profile">' +
              '<img class="cover" src="' + user.cover + '" alt="cover" />' +
              '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
              '<div class="data">' +
                '<h1 id="name">' + user.name + '<button class="logout btn-primary" onclick="facebookLogout()">Logout</button>' + '</h1>' +
                '<hr>' +
                '<h2>' + user.about + '</h2>' +
                '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
                '<hr>' +
              '</div>' +
            '</div>' +
            '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
            '<button class="postStatus btn-primary" onclick="postStatus()">Post ke Facebook</button>'
          );
        });

        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed" id="' + value.id + '">' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                  '<span class="btn btn-xs btn-primary" id="deletePost" onClick=deleteStatus("' + value.id + '")> Hapus Post</span>' +
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed" id="' + value.id + '">' +
                  '<h1>' + value.message + '</h1>' +
                  '<span class="btn btn-xs btn-primary" id="deletePost" onClick=deleteStatus("' + value.id + '")> Hapus Post</span>' +
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed" id="' + value.id + '">' +
                  '<h2>' + value.story + '</h2>' +
                  '<span class="btn btn-xs btn-primary" id="deletePost" onClick=deleteStatus("' + value.id + '")> Hapus Post</span>' +
                '</div>'
              );
            }
          });
        });
      } else {
        $('#lab8').html('<button class="login btn-primary" onclick="facebookLogin()">Login</button>');
      }
    };

    const facebookLogin = () => {
      FB.login(function(response){
        if (response.authResponse) {
          render(true);
        } else {
          console.log("login gagal");
        }
      }, {scope:'email,public_profile,user_posts,publish_actions,user_about_me', auth_type:'rerequest',});
    }

    const facebookLogout = () => {
      FB.logout(response => {
        render(false);
      })
    };

    const getUserData = (callback) => {
      FB.api('/me?fields=name,email,cover,picture,about,gender', response => {
        callback(response);
      });
    };

    const getUserFeed = (callback) => {
      FB.api('/me/feed/', 'GET', response => {
        callback(response);
      });
    };

    const postFeed = (message) => {
      FB.api('/me/feed/', "POST", {"message": message}, function (response) {
        window.location.reload();
      });
    };

    const postStatus = () => {
      const message = $('#postInput').val();
      postFeed(message);
    };

    const deleteStatus = (id) => {
        FB.api("/" + id , "DELETE", function (response) {
            if (response && !response.error) {
              $('#' + id).remove();
            }
          }
      );
    }
