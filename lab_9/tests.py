from django.test import TestCase, Client
from .views import profile
from .api_enterkomputer import get_drones
import environ

#requirements
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')


# Create your tests here.
class Lab9UnitTest(TestCase):
    def setUp(self):
        #GET ACCESS TOKEN
        self.client.post('/lab-9/custom_auth/login/',{"username":env('SSO_USERNAME'),"password":env('SSO_PASSWORD')},follow=True)

    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code,200)

    def test_lab_9_login_success(self):
        response = Client().post('/lab-9/custom_auth/login/',{"username":env('SSO_USERNAME'),"password":env('SSO_PASSWORD')},follow=True)
        response_html = response.content.decode('utf-8')
        self.assertEqual(response.status_code,200)

    def test_lab_9_login_failed(self):
        response = Client().post('/lab-9/custom_auth/login/',{"username":"user","password":"password"},follow=True)
        response_html = response.content.decode('utf-8')
        self.assertEqual(response.status_code,200)

    def test_lab_9_logout_success(self):
        response = Client().get('/lab-9/custom_auth/logout/')
        self.assertEqual(response.status_code,302)

    def test_add_item_success(self):
        drone = get_drones().json()
        drone_id = drone[0]['id']

        response = self.client.post('/lab-9/add_session_drones/{}/'.format(drone_id))
        self.assertEqual(response.status_code,302)

        drone_id = drone[1]['id']

        response = self.client.post('/lab-9/add_session_drones/{}/'.format(drone_id))
        self.assertEqual(response.status_code,302)

        response = self.client.get('/lab-9/',follow=True)
        self.assertEqual(response.status_code,200)

    def test_profile_error_handler_is_exist(self):
        response = Client().get('/lab-9/profile/',follow=True)
        self.assertEqual(response.status_code,200)

    def test_clear_item_success(self):
        #Set key
        session = self.client.session
        for key in ['drones','soundcards','opticals']:
            session[key] = []
        session.save()
        response = self.client.get('/lab-9/clear_session_drones/')
        self.assertEqual(response.status_code,302)

    def test_del_item_success(self):
        drone_id = '01'
        session  = self.client.session
        session['drones'] = [drone_id]
        session.save()

        response = self.client.get('/lab-9/del_session_drones/{}/'.format(drone_id))
        self.assertEqual(response.status_code,302)

    def test_cookie_login_exist(self):
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code,200)

    def test_cookie_login_success(self):
        response = Client().post('/lab-9/cookie/auth_login/',{'username':'utest','password':'ptest'},follow=True)
        self.assertEqual(response.status_code,200)

    def test_cookie_profile_can_be_accessed(self):
        response = self.client.get('/lab-9/cookie/profile/',follow=True)
        self.assertEqual(response.status_code,200)

    def test_cookie_auth_wrong_http_method(self):
        response = self.client.get('/lab-9/cookie/auth_login/',follow=True)
        self.assertEqual(response.status_code,200)

    def test_cookie_auth_wrong_password(self):
        response = self.client.post('/lab-9/cookie/auth_login/',{'username':'user','password':'pass'},follow=True)
        self.assertIn('Salah',response.content.decode('utf-8'))

    def test_clear_cookie_success(self):
        self.client.post('/lab-9/cookie/auth_login/',{'username':'utest','password':'ptest'})
        response = self.client.get('/lab-9/cookie/clear',follow=True)
        self.assertIn('Cookies direset',response.content.decode('utf-8'))
